from django.conf.urls import patterns, include, url

from tournament import views
from tournament import response

urlpatterns = patterns('',
    url(r'^$', views.manage, name='manager'),
    url(r'match_generator/$', response.match_generator, name = 'match_generator'),
    url(r'tournament/$', views.tournament, name='tournament'),
    url(r'wizzard/$', response.wizzard, name = 'wizzard'),
    url(r'generator/$', response.generator, name = 'generator'),    
    url(r'get_standings/$',response.get_standings,name = 'get_standings'),
    url(r'get_match_standings/$',response.get_match_standings,name = 'get_match_standings'),
    url(r'standings/$',response.standings,name = 'standings'),
    url(r'delete/$', response.ajaxDelete, name = 'delete'),    
    url(r'filter/$', response.ajaxFilter, name = 'filter'),
    url(r'list/$', response.ajaxList, name = 'list'),
    url(r'save/$', response.ajaxSave, name = 'save'),
    url(r'add/$', response.ajaxAdd, name = 'add'),
    url(r'save_match_results/$', response.save_match_results, name = 'save_match_results'),

    url(r'list/round$', views.tour, name='tour'),
    url(r'list/match$', views.round, name='round'),
    url(r'list/team$', views.match, name='match'),

    url(r'team/(?P<team_id>\d+)/$', views.team, name='team'),
    url(r'tour/(?P<tour_id>\d+)/$', views.tour, name='tour'),
    url(r'round/(?P<round_id>\d+)/$', views.round, name='round'),
    url(r'match/(?P<match_id>\d+)/$', views.match, name='match'),
    )