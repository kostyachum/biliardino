from django.shortcuts import render
from django.shortcuts import redirect
from tournament.models import Player, Match, Tournament, Round, Team, Vote
from tournament.forms import LoginForm, TourForm, RoundForm, MatchForm, RoundFullForm, TeamForm
from tournament.task import TaskClass
from django.db.models import Q
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
"""
Main page
"""
def index(request):
    tsk = TaskClass()
    login_form = LoginForm() 
    context = {}
    context['login_form'] = login_form
    context['players'] = Player.objects.all()
    
    try:
        tour = Tournament.objects.filter(status=1)[0]        
        current_tour = Tournament.objects.get(id=tour.id).round_set.all().order_by('serial')
        matches = tsk.getMatchesInTour(tour.id)
        players = tour.players.count()
        context['players_count'] = players
        context['matches_count'] = matches
        context['current_tour'] = current_tour
        context['tour'] = tour
    except:
        pass

    try:
        context['tours'] = Tournament.objects.filter(visible=True).all()
        #print context['tours']
    except:
        print("error while index")
    return render(request, 'tournament/user/user_index.html', context)

"""
Public tour page
"""
def index_tour(request, tour_id):     
    login_form = LoginForm() 
    tour = Tournament.objects.get(id=tour_id)
    round_list = Round.objects.filter(tournament=tour).all().order_by('serial')
    voted_percent = (100 / tour.players.count()) * tour.voted_players.count()
    top_list = []
    for plr in tour.players.all():
        try:
            vote = Vote.objects.get(player = plr, tour = tour)
            plr.rang = vote.rate
        except Vote.DoesNotExist:
            plr.rang = 10;
        top_list.append(plr)
    top_list = sorted(top_list, key=lambda x: x.rang, reverse = True)
    
    context = {"tour":tour, "round_list":round_list, 'login_form':login_form, 'voted_percent':voted_percent,'top_list': top_list[:5]}
    return render(request, 'tournament/user/user_tour.html', context)

"""
Public round page
"""
def index_round(request, round_id):
    login_form = LoginForm()
    rnd = Round.objects.get(id=round_id)
    match_list = Match.objects.filter(round=rnd)
    player_list = rnd.tournament.players.all()
    team_list = rnd.tournament.teams.all()
    context = {"match_list":match_list, "round":rnd, 'login_form':login_form, 'player_list':player_list, 'team_list':team_list}
    return render(request, 'tournament/user/user_round.html', context)

"""
Public team page
"""
def index_team(request, team_id):
    tsk = TaskClass()
    team = Team.objects.get(id=team_id)
    print("user id %s") % (request.user.id)
    
    context = {}
    context['team'] = team;
    context['goals'] = tsk.getTeamGoalAtAll(team_id)
    context['match_list'] = Match.objects.filter(Q(team_a=team) | Q(team_b=team)).filter(status=1)
    context['match_list_finished'] = Match.objects.filter(Q(team_a=team) | Q(team_b=team)).filter(status=2)
    try:
        plr = Player.objects.get(user_id=request.user.id)
        my_team = Team.objects.filter(players__in=[plr]).all()
        if team in my_team:
            #print("MY TEAM!!!!")
            context['my_team'] = True
    except:
        pass
    return render(request, 'tournament/user/user_team.html', context)

@login_required(login_url='/')
def index_vote(request, tour_id):    
    #print request.user.id
    player = Player.objects.get(user=request.user)
    tour_vots = Tournament.objects.exclude(id__in=(Tournament.objects.filter(voted_players__in=[player.id]))).filter(status=2, players__in=[player.id])
    tour = Tournament.objects.get(id=tour_id)
    player_list = tour.players.exclude(user = player.user)
    context = {}
    context['list'] = player_list
    if tour in tour_vots:    
        context["tour"] = tour
        return render(request, 'tournament/user/user_vote.html', context)
    else:
        return profile(request, request.user.id)
"""
User's profile
"""
def profile(request, player_id):
    player = Player.objects.get(user_id=player_id)
    teams = Team.objects.filter(players__in=[player.id])
    tours = Tournament.objects.filter(players__in=[player.id]).all()
    matches = Match.objects.filter(Q(team_a__in=teams) | Q(team_b__in=teams)).filter(status=2)
    # Not voted in
    tour_vots = Tournament.objects.exclude(id__in=(Tournament.objects.filter(voted_players__in=[player.id]))).filter(status=2, players__in=[player.id])
    context = {}
    context['tour_vots'] = tour_vots.all()
    context['user_teams'] = teams.all()
    context['player'] = player
    context['tours'] = tours
    context['matches'] = matches
    return render(request, 'tournament/user/profile.html', context)

"""
Manage page
"""
@staff_member_required
def manage(request):    
    if request.method == 'POST':
        form = TourForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/manage/")
    else:
        tours = Tournament.objects.all()
        form = TourForm()
        context = {'tours':tours, 'form': form}
        return render(request, 'tournament/manage/manage.html', context)

"""
Tour's page
"""
@staff_member_required
def tour(request, tour_id=-1):    
    if request.method == 'POST':
        form = RoundFullForm(request.POST)
        if form.is_valid():
            form.save()
            if (tour_id < 0):
                return redirect("/manage/tour/")  # Redirect after POST
            else:
                return redirect("/manage/tour/" + tour_id)  # Redirect after POST
    else:
        context = {}
        form = RoundFullForm()
        if (tour_id > 0):
            tour = Tournament.objects.get(id=tour_id)
            other_players = Player.objects.exclude(id__in=tour.players.all())
            round_list = Round.objects.filter(tournament=tour).all().order_by('serial')
            context = {"round_list":round_list, "form":form, "tour":tour, "other_players":other_players}
        else:
            round_list = Round.objects.all().order_by('serial')
            context = {"round_list":round_list, "form":form}
        return render(request, 'tournament/manage/tour.html', context)


"""
Round's page
"""
@staff_member_required
def round(request, round_id=-1):
    if request.method == "POST":
        form = MatchForm(request.POST)
        if form.is_valid():            
            play_off = request.POST.get('play_off_round')
            if (play_off != "0"):
                print("Save in play-off")
            #print(form.cleaned_data['play_date'])
            form.save()
            if (round_id < 0):
                return redirect("/manage/round/")  # Redirect after POST
            else:
                return redirect("/manage/round/" + round_id)  # Redirect after POST
        else:
            return redirect("/manage/list/match")  # Redirect after POST
    else:
        form = MatchForm()
        context = {}             
        if (round_id > 0):
            rnd = Round.objects.get(id=round_id)
            player_list = rnd.tournament.players.all()
            team_list = rnd.tournament.teams.all()   
            match_list = Match.objects.filter(round=rnd)
            context = {"match_list":match_list, "form":form, "round":rnd, "team_list":team_list, "player_list":player_list}
        else:
            match_list = Match.objects.all()               
            context = {"match_list":match_list, "form":form}
        return render(request, 'tournament/manage/round.html', context)

"""
Match's page
"""
@staff_member_required
def match(request, match_id=-1):
    if (request.method == 'POST'):
        form = TeamForm(request.POST)
        if form.is_valid():
            form.save()
            if match_id == -1:
                return redirect("/manage/list/team")  # Redirect after POST
            else:
                return redirect("/manage/match/%d") % (match_id)  # Redirect after POST
                
    else:
        form = TeamForm()
        if match_id > 0:
            match = Match.objects.get(id=match_id)
            return render(request, 'tournament/manage/match.html', {"match":match, 'form':form})  
        else:        
            return render(request, 'tournament/manage/match.html', {"team_list":Team.objects.all(), 'form':form})  

"""
Team's page
"""
@staff_member_required
def team(request, team_id=-1):
    if team_id > 0:
        team = Team.objects.get(id=team_id)
        return render(request, 'tournament/manage/team.html', {"team":team })
    else:
        return render(request, 'tournament/manage/team.html', {"team_list":Team.objects.all()})

"""
Tournament's page
"""
@staff_member_required
def tournament(request):
    if request.method == 'POST':
        form = TourForm(request.POST)
        if form.is_valid():
            """
            SAVE TOURNAMENT HERE 
            """
            return redirect("/")  # Redirect after POST
    else:
        t_form = TourForm()
        r_form = RoundForm()
        context = {
        'tform':t_form,
        'rform':r_form
        }
    return render(request, 'tournament/manage/tournament.html', context)       

def error_login(request):
    login_form = LoginForm() 
    return render(request, 'tournament/error.html', {"login_form":login_form})
