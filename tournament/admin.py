from django.contrib import admin
from tournament.models import Player,Team,Tournament,Round,Match, Vote

admin.site.register(Player)
admin.site.register(Team)
admin.site.register(Tournament)
admin.site.register(Match)
admin.site.register(Round)
admin.site.register(Vote)
# Register your models here.
