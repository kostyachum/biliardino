from django.test import TestCase
from tournament.models import User, Player, Team, Tournament, Round, Match
from tournament.task import TaskClass
class SomeTest(TestCase):
    
    def setUp(self):
        print("Setting up")
        
    def test_some_test(self):
        """
        First testing test
        """
        print("Test some test")
        usr = User(username="kostya")
        usr.set_password('123')
        usr.save()
        print("Created user with id = %d" % usr.pk)
        plr = Player(user=usr)
        plr.save()
        self.assertFalse(None, plr)
            
    def test_data(self):
        tsk = TaskClass()       
        pl1 = tsk.addPlayer(tsk.addUser("usr1").pk);
        pl2 = tsk.addPlayer(tsk.addUser("usr2").pk);
        pl3 = tsk.addPlayer(tsk.addUser("usr3").pk);
        pl4 = tsk.addPlayer(tsk.addUser("usr4").pk);
        
                
        team_a = Team()
        team_a.save() # Only after 'save' can add in ManyToMany fields
        team_a.team_name="TEAM A"
        team_a.players.add(pl1)
        team_a.players.add(pl2)
        team_a.save()
        
        team_b = Team()
        team_b.save()
        team_b.players.add(pl3)
        team_b.players.add(pl4)
        team_b.team_name="TEAM B"
        team_b.save()
                 
        tour = Tournament()
        tour.save()
        tour.players = Player.objects.all()
        tour.teams = Team.objects.all()
        round1 = Round(title = "Regular", round_type = 1, tournament = tour, serial = 1)
        round2 = Round(title = "Play-off", round_type = 2, tournament = tour ,serial = 2)
        
        round1.save()
        round2.save()
        
        match1 = Match(round = round1, title = "Match 1", status = 2)
        match1.team_a = team_a
        match1.team_b = team_b
        match1.score_a = 4
        match1.score_b = 1
        
        match2 = Match(round = round2, title = "Match 2", status = 2)
        match2.team_a = team_b
        match2.team_b = team_a
        match2.score_a = 0
        match2.score_b = 1
        
        match1.save()
        match2.save()
        tour.save()

        self.assertEquals(len(tsk.getTeamLooseInTour(tour.id,team_b.id)), 2)
        self.assertEquals(len(tsk.getTeamLooseInTour(tour.id,team_a.id)), 0)
        self.assertEquals(len(tsk.getTeamWinInTour(tour.id,team_b.id)), 0)
        self.assertEquals(len(tsk.getTeamWinInTour(tour.id,team_a.id)), 2)
        self.assertEquals(tour.players.count(), 4)
        self.assertEquals(tsk.getTeamGoalsInTour(tour.id,team_a.id), 5)