# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Player'
        db.create_table(u'tournament_player', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'tournament', ['Player'])

        # Adding model 'Team'
        db.create_table(u'tournament_team', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('team_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'tournament', ['Team'])

        # Adding M2M table for field players on 'Team'
        m2m_table_name = db.shorten_name(u'tournament_team_players')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('team', models.ForeignKey(orm[u'tournament.team'], null=False)),
            ('player', models.ForeignKey(orm[u'tournament.player'], null=False))
        ))
        db.create_unique(m2m_table_name, ['team_id', 'player_id'])

        # Adding model 'Tournament'
        db.create_table(u'tournament_tournament', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'tournament', ['Tournament'])

        # Adding M2M table for field players on 'Tournament'
        m2m_table_name = db.shorten_name(u'tournament_tournament_players')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tournament', models.ForeignKey(orm[u'tournament.tournament'], null=False)),
            ('player', models.ForeignKey(orm[u'tournament.player'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tournament_id', 'player_id'])

        # Adding model 'Round'
        db.create_table(u'tournament_round', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('tournament', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Tournament'])),
            ('serial', self.gf('django.db.models.fields.IntegerField')()),
            ('round_type', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'tournament', ['Round'])

        # Adding model 'Match'
        db.create_table(u'tournament_match', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('round', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Round'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('play_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('team_a', self.gf('django.db.models.fields.related.ForeignKey')(related_name='match_team_a', to=orm['tournament.Team'])),
            ('team_b', self.gf('django.db.models.fields.related.ForeignKey')(related_name='match_team_b', to=orm['tournament.Team'])),
            ('score_a', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('score_b', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.SmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'tournament', ['Match'])


    def backwards(self, orm):
        # Deleting model 'Player'
        db.delete_table(u'tournament_player')

        # Deleting model 'Team'
        db.delete_table(u'tournament_team')

        # Removing M2M table for field players on 'Team'
        db.delete_table(db.shorten_name(u'tournament_team_players'))

        # Deleting model 'Tournament'
        db.delete_table(u'tournament_tournament')

        # Removing M2M table for field players on 'Tournament'
        db.delete_table(db.shorten_name(u'tournament_tournament_players'))

        # Deleting model 'Round'
        db.delete_table(u'tournament_round')

        # Deleting model 'Match'
        db.delete_table(u'tournament_match')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'play_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'round': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Round']"}),
            'score_a': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'score_b': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'team_a': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match_team_a'", 'to': u"orm['tournament.Team']"}),
            'team_b': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'match_team_b'", 'to': u"orm['tournament.Team']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'tournament.player': {
            'Meta': {'object_name': 'Player'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'tournament.round': {
            'Meta': {'object_name': 'Round'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'round_type': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'serial': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.team': {
            'Meta': {'object_name': 'Team'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Player']", 'symmetrical': 'False'}),
            'team_name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Player']", 'symmetrical': 'False'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tournament']