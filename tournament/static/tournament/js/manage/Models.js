var PlayerModel = Base.extend({
	id: null,
	name: null,
	constructor: function(_id, _name) {
		this.id=_id;
		this.name = _name;
	}
});

var TeamModel = Base.extend({
	player_a: null,
	player_b: null,
	index: null,
	constructor: function(_index,a, b) {
		this.index = _index;
		this.player_a = a;
		this.player_b = b;
	},
	getName: function(){
		
			return this.player_a.name+" - "+this.player_b.name;
		
	}
});

var RoundModel = Base.extend({
	title: null,
	type: null,
	matсh_list: [],
	constructor: function(_title, _type){
		this.title = _title;
		this.type = _type;
	}
});

var MatchModel = Base.extend({
	title: null,
	team_a: null,
	team_b: null,
	round: null,
	constructor: function(_team_a, _team_b,_round){
		this.team_a = _team_a;
		this.team_b = _team_b;
		this.round = _round;
	},
	getName: function(ugly){
		if (ugly === undefined){
			name = "<b>"+this.team_a.getName()+"</b>&emsp;&emsp;<i>против</i>&emsp;&emsp;<b>"+this.team_b.getName()+"</b>"
			if ((this.title !== null) && this.title !== ""){
				return this.title+": "+name;
			}else{			
				return name;
			}
		}else{
			name = this.team_a.getName()+" против "+this.team_b.getName()
			if ((this.title !== null) && this.title !== ""){
				return this.title+": "+name;
			}else{			
				return name;
			}
		}
	}
});