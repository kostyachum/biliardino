/**
 * Приложение администратора
 * @lends biliardino.controller.ManageController
 */
biliardino.controller.ManageController = Base.extend({
	selected : null,
	/*
	 * Конструктор
	 */
	constructor : function() {
		console.log("-- ManageController: Конструктор.");
		var me = this;

		$("#show_reglament").click(function() {
			$("#reglament").toggle();
		});
		$("#showPlayerList").click(function() {
			$("#playerList").show("fast");
			$("#showPlayerList").hide("fast");
		});

		/*
		 * Resize multiselect for players
		 */
		$("#id_players").attr('size', '20');
		

		/*
		 * Delete player from tournament
		 */
		$(".delPlayerButton").click(function(evt) {
			player_id = evt.currentTarget.attributes.player.value;
			result = confirm("Удалить?");
			if (result) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "player_from_tour",
					"player_id" : player_id,
					"tour_id" : TOUR_ID
				};

				var succ = function() {
					location.reload();
				};

				me.ajax(AJAX_DELETE, data, succ, me.ajaxErr);
			};
		});

		$("#addPlayer").click(function() {
			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "addPlayerToTour",
				"player_id" : $("#playerList option:selected").val(),
				"tour_id" : TOUR_ID
			};

			var succ = function() {
				location.reload();
			};
			me.ajax(AJAX_ADD, data, succ, me.ajaxErr);

		});

		$("#filterRoundByTeam").click(function() {
			val = $("#filterRoundByTeam option:selected").val();
			if (val > -1) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "matchByTeam",
					"team_id" : val,
					"round_id" : ROUND_ID,
					"filter" : -1
				};
				var succ = function(a, b, c) {
					data = JSON.parse(a);
					html = me.generateMatchList(data);
					$("#match_list").html(html);

					me.setResultClick();
				};
				me.ajax(AJAX_FILTER, data, succ, me.ajaxErr());
			};
		});

		$("#filterRoundByPlayer").click(function() {
			val = $("#filterRoundByPlayer option:selected").val();
			if (val > -1) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "matchByPlayer",
					"player_id" : val,
					"round_id" : ROUND_ID,
					"filter" : -1
				};
				var succ = function(a, b, c) {
					data = JSON.parse(a);
					html = me.generateMatchList(data);
					$("#match_list").html(html);
					me.setResultClick();
				};
				me.ajax(AJAX_FILTER, data, succ, me.ajaxErr());
			};
		});
		me.setResultClick();
		/**
		 * Результаты матча
		 */
		$("#saveMatchResults").click(function(ev) {
			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"match_id" : me.match_id,
				"score_a" : $("#score_a").val(),
				"score_b" : $("#score_b").val(),
			};

			var succ = function(a) {
				me.res.html($("#score_a").val() + ":" + $("#score_b").val());
				$("#myModal").modal("hide");
				$("#score_a").val("");
				$("#score_b").val("");
			};

			me.ajax(SAVE_MATCH_ADR, data, succ, me.ajaxErr);
		});

		/**
		 * Включить редактирование
		 */
		$("#editBTN").click(function() {
			$("#show").toggle("fast");
			$("#edit").toggle("fast");
		});

		/**
		 * Перелючить таблицу
		 */
		$("#toggle-team-table").click(function() {
			$("#team_table").toggle("fast");
		});

		$("#doSearch").click(function() {
			me.doSearch();
		});

		$("#searchRoundByPlayer").click(function() {
			me.doMatchSearch();
		});

		$("#doMatchSearch").click(function() {
			me.doMatchSearch();
		});
		/**
		 * КНОПКА
		 */
		$("#saveTourBTN").click(function() {
			$("#show").show("fast");
			$("#edit").hide("fast");

			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "tour",
				"name" : $("#tourName").val(),
				"status" : $("#tourStatus").val(),
				"visible": $("#visible_id").prop('checked'),
				"id" : TOUR_ID
			};

			$("#tour_status").html($("#tourStatus option:selected").text());
			$("#tour_title").html($("#tourName").val());

			me.ajax(AJAX_SAVE, data, me.ajaxSucc, me.ajaxErr);
		});

		/**
		 * КНОПКА
		 */
		$("#saveRoundBTN").click(function() {
			$("#show").show("fast");
			$("#edit").hide("fast");

			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "round",
				"name" : $("#roundTitle").val(),
				"type" : $("#roundType").val(),
				"serial" : $("#roundSerial").val(),
				"active" : $("#id_active").prop('checked'),
				"id" : ROUND_ID
			};

			$("#round_type").html($("#roundType option:selected").text());
			$("#round_title").html($("#roundTitle").val());
			$("#round_serial").html($("#roundSerial").val());
			//console.log(data);
			me.ajax(AJAX_SAVE, data, me.ajaxSucc, me.ajaxErr);
		});

		/**
		 * КНОПКА СОХРАНЕНИЕ МАТЧА
		 */
		$("#saveMatchBTN").click(function() {
			$("#show").show("fast");
			$("#edit").hide("fast");

			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "match",
				"name" : $("#matchTitle").val(),
				"status" : $("#matchStatus option:selected").val(),
				"score_a" : $("#scoreA").val(),
				"score_b" : $("#scoreB").val(),
				"play_date" : $("#datepicker").val(),
				"playoff" : $("#playoff_selector option:selected").val(),
				"id" : MATCH_ID
			};

			$("#match_title").html($("#matchTitle").val());
			$("#score_a").html($("#scoreA").val());
			$("#score_b").html($("#scoreB").val());
			$("#match_status").html($("#matchStatus option:selected").text());
			$("#playoff_text").html($("#playoff_selector option:selected").text());
			me.ajax(AJAX_SAVE, data, me.ajaxSucc, me.ajaxErr);
			console.log(data);
		});

		/**
		 * КНОПКА СОХРАНЕНИЯ КОМАНДЫ
		 */
		$("#saveTeamBTN").click(function() {
			$("#show").show("fast");
			$("#edit").hide("fast");

			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "team",
				"team_name" : $("#teamTitle").val(),
				"active" : $("#activeSelect option:selected").val(),
				"avatar" : $("#avatar_link").val(),
				"file" : $("#file_to_upload").val(),
				"id" : TEAM_ID
			};

			$("#team_title").html($("#teamTitle").val());
			$("#active").html($("#activeSelect option:selected").text());

			me.ajax(AJAX_SAVE, data, me.ajaxSucc, me.ajaxErr);
		});

		//Добавление команды, привязанной к туру к матчу в слот А
		$(".add_to_a").click(function(a) {
			id = a.currentTarget.attributes.team.value;
			$("#id_team_a").val(id);
		});
		// ... в слот Б
		$(".add_to_b").click(function(a) {
			id = a.currentTarget.attributes.team.value;
			$("#id_team_b").val(id);
		});

		/**
		 * КНОПКА
		 * Генерация комманд
		 */
		$("#generateTeams").click(function() {
			$("#loadingImage").show("fast");
			var data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "tour_players",
				"tour_id" : TOUR_ID
			};
			var succ = function(answer) {
				// Получаем список игроков турнира
				js = JSON.parse(answer);
				console.log(js);
				var html = "";
				js.forEach(function(team, itter) {
					html += "<tr><td>" + team.fields.team_name + "</td></tr>";
				});
				$("#team_table").html(html);
				$("#loadingImage").hide();
			};

			var err = function(er, b, c) {
				$("#loadingImage").hide("fast");
				console.log(er, b, c);
				alert("Error while generating teams. More in console");
			};
			me.ajax(AJAX_LIST, data, succ, err);
			$("#team_generator").show("slow");
		});

		$("#generateMatches").click(function() {
			var count = $("#matches").val();
			var data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"count" : count,
				"round_id" : ROUND_ID
			};
			var succ = function(a) {
				location.reload();
			};
			me.ajax(AJAX_MATCH_GEN, data, succ, me.ajaxErr);
			console.log(count);
		});

		//DELETE ROUR BTN
		$("#removeTourBTN").click(function() {
			result = confirm("Удалить?");
			if (result) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "tour",
					"tour_id" : TOUR_ID
				};

				var succ = function() {
					history.go(-1);
				};

				me.ajax(AJAX_DELETE, data, succ, me.ajaxErr);
			};
		});

		// DELETE TEAM BTN
		$("#removeTeamBTN").click(function() {
			result = confirm("Удалить?");
			if (result) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "team",
					"team_id" : TEAM_ID
				};

				var succ = function() {
					history.go(-1);
				};

				me.ajax(AJAX_DELETE, data, succ, me.ajaxErr);
			};
		});
		// DELETE ROUND BTN
		$("#removeRoundBTN").click(function() {
			result = confirm("Удалить?");
			if (result) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "round",
					"round_id" : ROUND_ID
				};

				var succ = function() {
					history.go(-1);
					//window.history.back();
				};

				me.ajax(AJAX_DELETE, data, succ, me.ajaxErr);
			};
		});

		//DELETE MATCH BTN
		$("#removeMatchBTN").click(function() {
			result = confirm("Удалить?");
			if (result) {
				data = {
					"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
					"query" : "match",
					"match_id" : MATCH_ID
				};

				var succ = function() {
					history.go(-1);
				};

				me.ajax(AJAX_DELETE, data, succ, me.ajaxErr);
			};
		});
		/**
		 * ФИЛЬТРАЦИЯ ТУРОВ
		 */
		$("#tour_filter").click(function(a) {
			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "tour",
				"filter" : $("#tour_filter option:selected").val()
			};

			var succ = function(data, b, c) {
				var html = "";
				js = JSON.parse(data);
				/*
				 * <th>#</th>
				 <th>Title</th>
				 <th>Status</th>
				 <th>Players</th>
				 <th>Teams</th>
				 */
				js.forEach(function(item, itter) {
					html += "<tr><td>";
					html += item.pk;
					html += "<td><a href='/manage/tour/" + item.pk + "' > " + item.fields.title;
					html += "</a></td><td> " + item.fields.status;
					html += "</td><td></td><td></td></tr>";
				});
				$("#tour_table").html(html);
			};
			me.ajax(AJAX_FILTER, data, succ, me.ajaxErr);
		});

		/**
		 * ФИЛЬТРАЦИЯ РАУНДОВ
		 */
		$("#round_filter").click(function(a) {
			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "round",
				"id":TOUR_ID,
				"filter" : $("#round_filter option:selected").val()
			};

			var succ = function(data, b, c) {
				var html = "";
				js = JSON.parse(data);
				js.forEach(function(item, itter) {
					html += "<tr><td>";
					html += item.pk + "</td>";
					html += "<td> " + item.fields.serial + "</td>";
					html += "<td> <a href='manage/round/" + item.pk + "' >" + item.fields.title + "</td></a>";
					html += "<td> " + item.fields.round_type + "</td>";
					html += "</td></tr>";
				});
				$("#round_list").html(html);
			};
			me.ajax(AJAX_FILTER, data, succ, me.ajaxErr);
		});

		/**
		 * ФИЛЬТРАЦИЯ Матчей
		 */
		$("#match_filter").click(function(a) {
			data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"query" : "match",
				"filter" : $("#match_filter option:selected").val(),
				"id" : ROUND_ID
			};

			var succ = function(data, b, c) {
				var html = "";
				data = JSON.parse(data);
				$("#match_list").html(me.generateMatchList(data, false));
				me.setResultClick();
			};
			me.ajax(AJAX_FILTER, data, succ, me.ajaxErr);
		});

		/**
		 * Включение/выключение таблицы
		 */
		$("#toggleTable").click(function() {
			$("#table-playoff").toggle();
			var data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"round_id" : ROUND_ID
			};

			var succ = function(data) {
				js = JSON.parse(data);
				var ar = [];
				ar[0] = [];
				ar[1] = [];
				ar[2] = [];
				ar[3] = [];
				js.forEach(function(a, b) {
					js2 = JSON.parse(a);
					js2.forEach(function(x, y) {
						ar[b][y] = x;
					});
				});

				$(".playoff-box[playoff=8] select").each(function(i, a) {
					console.log(a, i);
					if (i < ar[0].length) {
						$(a).attr("value", ar[0][i]);
					} else {
						return false;
					};
				});

				$(".playoff-box[playoff=4] select").each(function(i, a) {
					console.log(a, i);
					if (i < ar[1].length) {
						$(a).attr("value", ar[1][i]);
					} else {
						return false;
					};
				});

				$(".playoff-box[playoff=2] select").each(function(i, a) {
					console.log(a, i);
					if (i < ar[2].length) {
						$(a).attr("value", ar[2][i]);
					} else {
						return false;
					};
				});

				$(".playoff-box[playoff=1] select").each(function(i, a) {
					console.log(a, i);
					if (i < ar[3].length) {
						$(a).attr("value", ar[3][i]);
					} else {
						return false;
					};
				});
			};
			me.ajax(AJAX_GET_STANDINGS, data, succ, me.ajaxErr);
		});

		/**
		 * Сохранение сетки Play-off
		 */
		$("#save-table").click(function() {
			var web8 = $(".playoff-box[playoff=8] select :selected");
			var web4 = $(".playoff-box[playoff=4] select :selected");
			var web2 = $(".playoff-box[playoff=2] select :selected");
			var web1 = $(".playoff-box[playoff=1] select :selected");
			var arr8 = [];
			var arr4 = [];
			var arr2 = [];
			var arr1 = [];

			web8.each(function(a, b) {
				if (b.attributes.value) {
					arr8.push(b.attributes.value.value);
				};
			});

			web4.each(function(a, b) {
				if (b.attributes.value) {
					arr4.push(b.attributes.value.value);
				};
			});

			web2.each(function(a, b) {
				if (b.attributes.value) {
					arr2.push(b.attributes.value.value);
				};
			});

			web1.each(function(a, b) {
				if (b.attributes.value) {
					arr1.push(b.attributes.value.value);
				};
			});

			var data = {
				"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
				"a8" : JSON.stringify(arr8),
				"a4" : JSON.stringify(arr4),
				"a2" : JSON.stringify(arr2),
				"a1" : JSON.stringify(arr1),
				"round_id" : ROUND_ID

			};
			me.ajax(AJAX_STANDINGS, data, me.ajaxSucc, me.ajaxErr);
			console.log(data);
			$("#table-playoff").toggle();
		});

		$("#searchByPlayer").keypress(function(e) {
			if (e.which == 13) {
				me.doSearch();
			}
		});

		$("#searchMatchByPlayer").keypress(function(e) {
			if (e.which == 13) {
				me.doMatchSearch();
			}
		});
	},
	setResultClick: function(){
		var me = this;
		/**
		 * Включение окна установки результатов матча
		 */
		$(".set-results").click(function(env) {
			console.log(env);
			me.match_id = env.currentTarget.attributes.match.value;

			me.res = $(env.currentTarget.parentElement);
			console.log("team_a", $("#team_a" + me.match_id));

			team_a_id = env.currentTarget.attributes.team_a.value;
			team_b_id = env.currentTarget.attributes.team_b.value;
			title_a = env.currentTarget.attributes.title_b.value;
			title_b = env.currentTarget.attributes.title_b.value;

			$("#team_a_name").html("<a title = '" + title_a + "' href='/team/" + team_a_id + "'>" + $("#team_a"+me.match_id)[0].innerText + "</a>");
			$("#team_b_name").html("<a title = '" + title_b + "' href='/team/" + team_b_id + "'>" + $("#team_b"+me.match_id)[0].innerText + "</a>");
			$('#myModal').modal();
		});

	},
	/*
	 * Поиск игроков
	 */
	doSearch : function() {
		search_text = $("#searchByPlayer").val();
		var data = {
			"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
			"query" : "team",
			"filter" : 0,
			"search" : search_text
		};

		var succ = function(a, b, c) {
			data = JSON.parse(a);
			table = $("#team_list");
			html = "";
			data.forEach(function(a, b) {
				console.log(a, b);
				html += "<tr><td><a href = '/manage/team/" + a.pk + "'>#" + a.pk + ": " + a.fields.team_name + "</a></td></tr>";
				console.log(b);
			});
			table.html(html);
		};

		this.ajax(AJAX_FILTER, data, succ, this.ajaxErr);
	},
	/**
	 * Поиск по матчей по игрокам
	 */
	doMatchSearch : function() {
		search_text = $("#searchMatchByPlayer").val();
		var data = {
			"csrfmiddlewaretoken" : $("input[name$='csrfmiddlewaretoken']").val(),
			"query" : "match",
			"search" : search_text,
			"id" : ROUND_ID
		};

		var succ = function(a, b, c) {
			var me = this;
			data = JSON.parse(a);
			console.log(data);
			table = $("#match_list");
			html = me.generateMatchList(data, true);
			table.html(html);

			me.setResultClick();
		};

		this.ajax(AJAX_FILTER, data, succ, this.ajaxErr);
	},
	generateMatchList : function(data, res_button) {
		html = "";
		data.forEach(function(a, b) {
			html += "<tr>";
			html += "<td>" + a.pk + "</td>";
			if (USER_SIDE) {
				html += "<td>" + a.fields.title + "</td>";
			} else {
				html += "<td ><a href='/manage/match/" + a.pk + "'>" + a.fields.title + "</a></td>";
			};

			html += "<td id='team_a" + a.pk + "'> <a href = '/team/" + a.fields.team_a.pk + "'>" + a.fields.team_a.fields.team_name + "</a></td>";
			html += "<td>";
			if (a.fields.status == 2) {
				html += a.fields.score_a + ":" + a.fields.score_b;
			} else {
				if (!USER_SIDE) {/*
					 <button class="btn btn-mini set-results"
					 match="{{ match.id}}"
					 team_a="{{match.team_a.id}}" t
					 eam_b="{{match.team_b.id}}"
					 title_a="{{match.team_a}}"
					 title_b="{{match.team_b}}">
					 */
					html += "<button class='btn btn-mini set-results'  match='" + a.pk + "' team_a='" + a.fields.team_a.pk + "' team_b='" + a.fields.team_b.pk + "' title_a='" + a.fields.team_a.fields.team_name + "' title_b='" + a.fields.team_b.fields.team_name + "'>RES</button>";
				};
			};
			html += "</td>";

			html += "<td id='team_b" + a.pk + "'> <a href = '/team/" + a.fields.team_b.pk + "'>" + a.fields.team_b.fields.team_name + "</a></td>";

			if (a.fields.status === 0) {
				html += "<td><label class='label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> </td>";
			};

			if (a.fields.status === 1) {
				html += "<td><label class='label label-info'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>";
			};

			if (a.fields.status === 2) {
				html += " <td><label  class='label label-success'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> </td> ";
			};

			html += "</tr>";
		});
		return html;
	},
	/*
	 * Заглушка для Ajax-success
	 */
	ajaxSucc : function(a, b, c) {
		console.log(a, b, c);
	},
	/*
	 * Заглушка для Ajax-error
	 */
	ajaxErr : function(a, b, c) {
		console.log(a, b, c);
	},
	/**
	 * AJAX-запрос
	 */
	ajax : function(addres, data, succ, err) {
		$.ajax({
			type : 'post',
			async : 'true',
			url : addres,
			data : data,
			dataType : "json",
			error : err,
			success : succ
		});
	}
});
