/**
 * Приложение визарда создания турниров
 * @lends biliardino.controller.ManageController
 */
biliardino.controller.TourController = Base.extend({
    team_list: [],
    round_list: [],
    player_m_list: [],
    total_matches: 0,
    /*
     * Конструктор
     */
    constructor: function() {
        console.log("-- TourController: Конструктор.");
        var me = this;

        var nextRoundButton = $("#nextRound");
        var prevTourButton = $("#prevTour");
        var generateTeamsButton = $("#generateTeams");
        var saveButton = $("#saveAndAdd");
        var nextMatchButton = $("#nextMatch");
        var prevRound = $("#prevRound");
        var generateMatch = $("#generateMatch");
        var doneButton = $("#doneButton");
        var prevMatch = $("#prevMatch");
        var saveAll = $("#saveAll");
        var cancelAddMatchButton = $("#cancelAddMatch");
        var addMatchToPlayOff = $("#addMatchToPlayOff");

        /**
        *
        */
        nextRoundButton.click(function(){
        	$("#newTournament").slideToggle("fast");
        	$("#newRound").slideToggle("fast");
        });

        /**
        *
        */
        prevTourButton.click(function(){        
        	$("#newTournament").slideToggle("fast");
        	$("#newRound").slideToggle("fast");
        
        });

        /**
        * Кнопка генерации комманд
        */
        generateTeamsButton.click(function(){
        	console.log(" --Generate Teams");
            me.player_m_list = [];
        	play_list = $("#id_players option:selected");        	
        	play_list.each(function(itter,b){        		
        		me.player_m_list[$(b).attr("value")]=(new PlayerModel($(b).attr("value"), $(b).html()));                
        	});
            me.player_m_list.len = play_list.length;
            console.log(me.player_m_list);
        	me.generateTeams();
        });

        /**
        *
        */
        saveButton.click(function(){
        	var rnd = new RoundModel($("#newRound #id_title").val(),$("#id_round_type").val());
        	console.log(rnd);
        	me.round_list.push(rnd);
        	
        	me.renderRoundList();
        });

        /**
        *
        */
        nextMatchButton.click(function(){
        	if (me.round_list.length === 0){
        		$("#roundMesssage").show("fast");
        	}else{
				$("#newRound").slideToggle();
				me.sortRounds();
        		me.generateMatchSlide();
        	}
        });

        /**
        *
        */
        prevRound.click(function(){
        	$("#newRound").slideToggle("fast");
        	$("#newMatch").slideToggle("fast");
        });

        /**
        *
        */
        generateMatch.click(function(){
        	me.generateMatchForRounds();
        });

        /**
        *
        */
        doneButton.click(function(){
        	$("#newMatch").slideToggle("fast");
        	$("#results").slideToggle("fast");
        	me.generateResulrs();
        });

        /**
        *
        */
        prevMatch.click(function(){
        	$("#newMatch").slideToggle("fast");
        	$("#results").slideToggle("fast");
        });

        /**
        *
        */
        saveAll.click(function(){
        	me.collectAndSaveData();
        });

        /**
        *
        */
        cancelAddMatchButton.click(function(){
        	$("#addPlayOffForm").slideToggle();
        });

        /**
        *
        */
        addMatchToPlayOff.click(function(){
        	me.addMatch();
        });
    },
    /**
    * Сборка и сохранение все данных
    * из моделей формируется большой JSON
    */
    collectAndSaveData: function(){
    	$("#loading").show("fast");
    	$("#resultsControls").hide("fast");
    	var me = this;
    	var wizData = {};
    	var players_id = [];
    	
    	wizData["csrfmiddlewaretoken"] = $("input[name$='csrfmiddlewaretoken']").val();
    	wizData["title"] = $("#id_title").val();
    	wizData["status"] = $("#id_status option:selected").val();
    	tour_players = {};
    	tour_rounds = {};
    	tour_teams = {};
    	//List of players for send on server
    	me.player_m_list.forEach(function(plr, itter){
    		var play_array = {
    			"id":plr.id,
    			"name": plr.name
    		};
    		tour_players[itter] = play_array;   		
    	});

    	//List of round to send on server
    	me.round_list.forEach(function(round,itter){
    		var matсh_list = {};

    		//List of match for round
    		round.matсh_list.forEach(function(m,i){
    			matсh_list[i] = {
    				"name":m.getName(true),    				
    				"team_a": m.team_a.index,
    				"team_b":m.team_b.index
    			};
    		});

    		// Round info
			round_data ={	
				"serial": itter,
    			"title":round.title,
    			"type": round.type,
    			"match_list": matсh_list
    		};
    		tour_rounds[itter] = round_data;
    	});

    	me.team_list.forEach(function(t,i){
    		tour_teams[i] = {
    			"a":t.player_a.id,
    			"b":t.player_b.id,
    			"index": t.index
    		};
    	});


    	wizData['players'] = JSON.stringify(tour_players);
		wizData['rounds'] = JSON.stringify(tour_rounds);
		wizData['teams'] = JSON.stringify(tour_teams);
    	var error = function(a,b,c){
    		$("#loading").hide("fast");
    		$("#resultsControls").show("fast");
    		console.log(a,b,c);
    		alert("Ошибка при сохранение. Плохо.");
    	};

    	var succ = function(a,b,c){
    		$("#loading").hide("fast");
    		$("#saveSuccess").show("fast");
    	};

    	console.log(wizData);
    	$.ajax({
    		type: 'post',
            async: 'true',
            url: wizzard_addres,
            data: wizData,
            dataType: "json",
            error: error,
            success: succ
    	});


    },
    /**
    * Приведение списка раундов в соответствие с сортировщиком
    */
    sortRounds: function(){
    	var me = this;
    	var sortedRound = $(".round_itter");
    	sorted_round_list = [];
    	sortedRound.each(function(i,item){
    		var new_itter = parseInt(item.innerHTML);
    		sorted_round_list[i]=me.round_list[new_itter];
    	});

    	me.round_list = sorted_round_list;
    	console.log("sorted:", me.round_list);
    },
    /**
    * Добавление матча в Play-off раунд
    */
    addMatch: function(){
    	var me = this;
    	var title = $("#poff_match_title").val();
    	var round_id = $("#poff_round_id").val();
    	var team_a = $("#poff_team_a_select").val();
    	var team_b = $("#poff_team_b_select").val();

    	var ok = true;

    	if (team_a === team_b){
    		ok = false;
    		alert("Нужны две разные комманды!");
    	};

    	if (title === "") {
    		ok = false;
    		alert("Введи название раунда");
    	};

    	if (ok){
    		new_match = new MatchModel();
    		
    		new_match.team_a = me.team_list[team_a];  
    		new_match.title = title;  		
    		new_match.team_b = me.team_list[team_b];
    		new_match.round = me.round_list[round_id];
    		
    		console.log("new match", new_match);
    		me.round_list[round_id].matсh_list.push(new_match);

    		me.generateMatchForRounds();
    	};
    	console.log("-------------");
    	console.log(title, round_id,team_a, team_b);
    	console.log("-------------");
    },
    /**
    * Генерация результатов.
    * Последний слайд перед сохранением в бд
    */
    generateResulrs: function(){
    	var titleHtml = "";
    	titleHtml += ("<div class='span5'>Название: <span>"+$("#id_title").val()+"</span>");
    	titleHtml += ("<br>Статус: <span>"+$("#id_status option:selected").text()+"</span></div>");
    	titleHtml += ("<div class='span3'>Комманд: "+this.team_list.length);
    	titleHtml += ("<br>Раундов: "+this.round_list.length);
    	titleHtml += ("<br>Всего матчей: "+this.total_matches+"</div>");
		
		$("#tourTitle").html(titleHtml);
		$("#tourMatchList").html("");
		$("#tourPlayerList").html("");

    	this.round_list.forEach(function(round, i){
    		var tblHtml = "";
    		if (round.matсh_list){
    		
	    		round.matсh_list.forEach(function(match, i){
	    			tblHtml+=("<tr><td>" + match.getName() + "</td></tr>");
	    		});
    		};

    		$("#tourMatchList").append("<table class='table manage-table'><caption class='teableHeader'>"
    			+ round.title + "</caption>" + tblHtml + "</table>");

    	});

    	this.team_list.forEach(function(team,i){
    		$("#tourPlayerList").append("<tr><td>"+team.getName()+"</td></tr>");
    	});
    },
    /**
    * Генерит и рендерит матчи
    */
    generateMatchForRounds: function(){
    	var me = this;
    	var roundLongList = $(".roundLognList");
    	me.total_matches=0;
    	$("#matchHolder").html("");
    	roundLongList.each(function(itter, elem){
			// console.log($(elem).attr('title'));
    		var count = $(elem).children().val();
    		console.log(count);
    		if (count){
    			var matches = [];
    			// генерации матчей
    			// каждая команда получает указанное количество матчей
    			var inc = 0;
    			for (var i=0;i<count;i++){    				
    				if (inc == 2){
	    					inc = 0;
        				} 
        			inc ++;
    				me.team_list.forEach(function(team,itter_a){
    				me.team_list.forEach(function(team_b,itter_b){
    					if (itter_a>itter_b){
    						var m = new MatchModel();
    						if (inc == 2){
    							m.team_a = team;
    							m.team_b = team_b;	
    						}else{
    							m.team_b = team;
    							m.team_a = team_b;
    						}
    						
    						m.round = me.round_list[itter]
    						matches.push(m);
    					};
    				});
    			});
    			};
    			me.round_list[itter].matсh_list = matches;
    		};
    	});

    	me.round_list.forEach(function(round, itter){
				var html = "<table class='table'>";
				html+="<caption>"+round.title+"</caption>";	    		
	    		round.matсh_list.forEach(function(m,i){
						html+=("<tr><td>"+m.getName()+"</td></tr>");
						me.total_matches++;
	    			});
	    		html+="</table>"
	    		$("#matchHolder").append(html);
			});
    },
    /**
    * Рендерить список раундов на страничке генерации матчей
    */
    generateMatchSlide: function(){
    	var matchSlide = $("#newMatch");
    	var generatedHolder = $("#generated");

		generatedHolder.html("Количество матчей:");
    	this.round_list.forEach(function(round, itter){
    		console.log(round);
    		if (round.type === "0"){
    		var html = "";
    		html += "<div class='roundLognList' title='"+round.title+"'>" + round.title
    		+ ": <input type='text'  class='input-small'></div>";
    		generatedHolder.append(html);
    	}else{
				var html = "";
    			html += "<div class='roundLognList' title='"
    			+ round.title + "'>" + round.title
    			+ ": playf-off <button  class='addMatchToRound' round_id='"
    			+ itter + "'><i class='icon-plus-sign' ></i></button></div>";
    			generatedHolder.append(html);
    		};
    	});

    	$(".addMatchToRound").click(function(a){
    		round_id = a.currentTarget.attributes.round_id.value;
    		$("#poff_round_id").val(round_id);
    		$("#addPlayOffForm").slideToggle("slow");    		
    		console.log("round id",round_id);
    	});
    	matchSlide.slideToggle();
    },
    /**
    * Генерация команд
    */
    generateTeams: function(){
    	var me = this;
        console.log("Team generator");
    	ln = me.player_m_list.len;
        //Проверяем на четность
    	if (((ln % 2) != 0) || (ln < 4)){
    		me.showMessage(ln);
    	}else{
    		me.team_list = [];
    		// Разбор сгенерированых на сервере комманд
            var succ = function(data){
            console.log(me.player_m_list);    
                data.forEach(function(item, itter){

                    var player_a = me.player_m_list[item[0]];
                    var player_b = me.player_m_list[item[1]];

                    var team = new TeamModel(itter,player_a,player_b)                    
                    me.team_list.push(team);
                });

                me.renderTeamList();
            };

            var data = {
                 "csrfmiddlewaretoken":$("input[name$='csrfmiddlewaretoken']").val()
            };
	
			var err = function(){
				alert("Не удалось сгенерировать команды на сервере");
			};
            players = [];
            
            me.player_m_list.forEach(function(a,i){
                 players.push(a.id);
            });
            data['players'] = JSON.stringify(players);
            console.log(data);
            me.ajax(AJAX_TEAMS,data,succ, err);
    		
    	};

    	team_a_select = $("#poff_team_a_select");
		team_b_select = $("#poff_team_b_select");
		console.log("team select",team_b_select);
		me.team_list.forEach(function(team,itter){
			team_a_select.append("<option value='"+itter+"'>"+team.getName()+"</option>");
			team_b_select.append("<option value='"+itter+"'>"+team.getName()+"</option>");
		});

    },
    /**
    * Вывод списка команды
    */
    renderTeamList:function(){
    	console.log("Team list render");
    	var me = this;
    	var listHolder = $("#teamList");
		listHolder.html("");
    	listHolder.append(" <caption class='teableHeader'>Команды:</caption>");
    	me.team_list.forEach(function(team, itter){
    		listHolder.append("	<tr><td>"
    			+team.player_a.name+"</td>	<td>"
    			+team.player_b.name+"</tr>	");    		
    	});
    },
    /**
    * Вывод списка раундов
    */
    renderRoundList: function(){
    	console.log("Round list render");
    	var me = this;
    	var listHolder = $("#sortable");
    	listHolder.html("");
    	me.round_list.forEach(function(round, itter){
    		listHolder.append(
    			"<li class='ui-state-default'"
    			+ "<span class='ui-icon ui-icon-arrowthick-2-n-s'> </span><txt class='round_itter'>" +itter+
				"</txt><txt class='round_title'>: "+round.title+"</txt>	,<txt class='round_type'>"
				+ round.type + "</txt><button class='btn pull-right deleteRound' roun_id = '"+itter+"'style='margin:0px'><i class = 'icon-remove-sign'></i></button></li>");
    	});

    	$(".deleteRound").click(function(a){
    		console.log(me.round_list);
    		round_id = console.log("round to delete:");
    		me.round_list.splice(round_id, 1);
    		me.renderRoundList();
    		console.log(me.round_list);
    	});
    	$( "#sortable" ).sortable();
    	$( "#sortable" ).disableSelection();
    },
    /**
    * Сообщение об нечетном числе игроков
    */
    showMessage:function(ln){
    	$("#teamsError").append("<br> Выбранно:"+ln);
    	$("#teamsError").show("fast");
    },
    /*
    * Заглушка для Ajax-success
    */
    ajaxSucc: function(a,b,c){
        console.log(a,b,c);
    },
    /*
    * Заглушка для Ajax-error
    */
    ajaxErr: function(a,b,c){
        console.log(a,b,c);
    },
    /**
    * AJAX-запрос
    */
    ajax: function(addres,data,succ,err){
        $.ajax({
            type: 'post',
            async: 'true',
            url: addres,
            data: data,
            dataType: "json",
            error: err,
            success: succ
        });
    }
});