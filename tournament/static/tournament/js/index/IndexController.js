biliardino.controller.IndexController = Base
		.extend({
			constructor : function() {
				var me = this;
				console.log("--- index controller ");

				$("#userName").keypress(function(e) {
					if (e.which == 13) {
						me.searchPlayers();
					}
				});

				$("#doSearchUser").click(function() {
					me.searchPlayers();
				});
				$("#sendVote").click(
						function() {
							$("#loading").show();
							$("#notLoading").hide();
							$("#sendVote").attr("disabled", "disabled");
							list = [];
							listNode = $("#sortable li");
							listNode.each(function(a, b) {
								list.push($(b).attr('player_id'));
							});
							data = {
								"csrfmiddlewaretoken" : $(
										"input[name$='csrfmiddlewaretoken']")
										.val(),
								"list" : JSON.stringify(list)
							};

							var succ = function(a) {
								alert("Ваш голос принят");
								window.location = "/profile/" + USER_ID;
							};

							console.log(data, AJAX_SAVE_VOTE);
							me.ajax(AJAX_SAVE_VOTE, data, succ, me.ajaxErr);
						});

				$(".round-title").click(function(a) {
					var id = a.currentTarget.attributes.round.value;
					me.loadPlayOffStandingsIndex(id);
					$("#round" + id).slideToggle("slow");
				});

				$(".round-title").each(function(itter, element) {
					console.log("AAA");
					id = $(element).attr("round");

					me.loadPlayOffStandingsIndex(id);
				});

				$("#editBTN").click(function() {
					$("#show").toggle("fast");
					$("#edit").toggle("fast");
				});

				/*
				 * 
				 */
				$("#savePlayerButton")
						.click(
								function() {
									data = {
										"csrfmiddlewaretoken" : $(
												"input[name$='csrfmiddlewaretoken']")
												.val(),
										"query" : "player",
										"first" : $("#player_first_name").val(),
										"last" : $("#player_last_name").val(),
										"email" : $('#player_email').val(),
										"player_id" : PLAYER_ID
									};

									var succ = function(data) {
										$("#actual_name")
												.html(
														$("#player_first_name")
																.val()
																+ " "
																+ $(
																		"#player_last_name")
																		.val());
										$("#actual_email").html(
												$('#player_email').val());

										$("#show").toggle("fast");
										$("#edit").toggle("fast");
									};

									var err = function(data) {
										alert("error!", data);
									};

									me.ajax(AJAX_SAVE, data, succ, err);
								});

				/*
				 * FACEBOOK login button
				 */
				$(".facebookbutton")
						.click(
								function() {
									FB
											.login(
													function(respones) {
														console.log(respones);
														if (respones.status === "connected") {
															user_id = respones.authResponse.userID;
															photo = "";
															FB
																	.api(
																			'/'
																					+ user_id
																					+ "/picture",
																			function(
																					response) {
																				photo = response;
																			});

															FB
																	.api(
																			'/'
																					+ user_id,
																			function(
																					response) {
																				console
																						.log(response);
																				response.userID = user_id;
																				response.photo = photo;
																				me
																						.facebookCheck(response);
																			});

														} else {
															alert("Не удалось авторизироваться через Facebook");
														}
														;
													},
													{
														scope : "user_photos, friends_photos, email"
													});
								});

				console.log("Login controller");
				$.ajaxSetup({
					cache : true
				});

				$.getScript('//connect.facebook.net/en_UK/all.js', function() {
					FB.init({
						appId : '1437314539831418',
					});
					$('.facebookbutton').removeAttr('disabled');
				});

				$.getScript('//vk.com/js/api/openapi.js', function() {
					VK.init({
						apiId : 4111294
					});
					$('.vk-login-button').removeAttr('disabled');
				});

				$(".vk-login-button")
						.click(
								function() {
									VK.Auth
											.login(function(response) {
												if (response.session) {
													// Если удалось
													// авторизирвоаться
													vk_avatar = "";
													// Получаем данные через API
													VK
															.api(
																	"users.get",
																	{
																		"uids" : response.session.mid,
																		"fields" : "photo"
																	},
																	function(
																			data) {
																		/*
																		 * SEND
																		 * QUERY
																		 */
																		vk_avatar = data.response[0].photo;

																		data = {
																			"csrfmiddlewaretoken" : $(
																					"input[name$='csrfmiddlewaretoken']")
																					.val(),
																			"first_name" : response.session.user.first_name,
																			"last_name" : response.session.user.last_name,
																			"photo" : vk_avatar,
																			"vk_id" : response.session.mid,
																			"vk_href" : response.session.user.href
																		};

																		var succ = function(
																				data) {
																			window.location = "/";
																			console
																					.log(data);
																		};

																		var err = function(
																				data) {
																			console
																					.log(data);
																			alert("Не удалось авторизироваться через Vk.com");
																		};
																		me
																				.ajax(
																						VKONTAKTE_LOGIN,
																						data,
																						succ,
																						err);
																	});

												} else {
													alert('not auth');
												}
											});
								});

				/**
				 * ФИЛЬТРАЦИЯ ТУРОВ
				 */
				$("#tour_filter").click(
						function(a) {
							data = {
								"csrfmiddlewaretoken" : $(
										"input[name$='csrfmiddlewaretoken']")
										.val(),
								"query" : "tour",
								"filter" : $("#tour_filter option:selected")
										.val()
							};

							var succ = function(data, b, c) {
								var html = "";
								js = JSON.parse(data);
								js.forEach(function(item, itter) {
									html += "<tr><td>";
									html += item.pk;
									html += "<td><a href='/manage/tour/"
											+ item.pk + "' >"
											+ item.fields.title;
									html += "</a></td><td> "
											+ item.fields.status;
									html += "</td><td></td><td></td></tr>";
								});
								$("#tour_table").html(html);
							};
							me.ajax(AJAX_FILTER, data, succ, me.ajaxErr);
						});
			},
			/**
			 * Loading Facebook data
			 */
			facebookCheck : function(resp) {
				var me = this;
				var data = {
					"csrfmiddlewaretoken" : $(
							"input[name$='csrfmiddlewaretoken']").val(),
					"facebook_id" : resp.userID,
					"first_name" : resp.first_name,
					"last_name" : resp.last_name,
					"email" : resp.email,
					"photo" : resp.photo.data.url
				};

				var succ = function(resp) {
					console.log("facebook login", resp);
					if (resp === "login") {
						window.location = "/";
					}
					;

					if (resp === "register") {
						alert("Вы успешно зарегестрированы через Facebook");
						window.location = "/";
					}
					;
				};
				console.log(data);
				var err = function(err) {
					console.log("facebook error", err);
				};
				me.ajax(FACEBOOK_LOGIN, data, succ, err)
			},
			loadPlayOffStandingsIndex : function(round_id) {
				/*
				var me = this;
				var data = {
					"csrfmiddlewaretoken" : $(
							"input[name$='csrfmiddlewaretoken']").val(),
					"round_id" : round_id
				};

				var succ = function(data) {

					var ar = data;
					console.log(data);
					var eight = $(".playoff-box[playoff=8]");
					var four = $(".playoff-box[playoff=4]");
					var two = $(".playoff-box[playoff=2]");
					var one = $(".playoff-box[playoff=1]");

					eight.html("");
					four.html("");
					two.html("");
					one.html("");

					ar[0].forEach(function(a, x) {
						eight.append("<div class='match-box'>" + ar[0][x]
								+ "<br></div>");
					});

					ar[1].forEach(function(a, x) {
						four.append("<div class='match-box'>" + ar[1][x]
								+ "<br></div>");
					});

					ar[2].forEach(function(a, x) {
						two.append("<div class='match-box'>" + ar[2][x]
								+ "<br></div>");
					});

					ar[3].forEach(function(a, x) {
						one.append("<div class='match-box'>" + ar[3][x]
								+ "<br></div>");
					});

				};
				me.ajax(AJAX_GET_STANDINGS_INDEX, data, succ, me.ajaxErr);
*/
			},
			searchPlayers : function() {
				var me = this;
				data = {
					"csrfmiddlewaretoken" : $(
							"input[name$='csrfmiddlewaretoken']").val(),
					"search" : $("#userName").val(),
					"query" : "player",
					"filter" : -1
				};

				var succ = function(a, b, c) {
					data = JSON.parse(a);
					console.log(data);
					tbl = $("#userList");
					html = "";
					data.forEach(function(a, b) {
						html += "<tr><td><a href = '/profile/"
								+ a.fields.user.pk + "'>";
						html += a.fields.user.fields.first_name + " ";
						html += a.fields.user.fields.last_name;
						html += "</a></td></tr>";
					});
					console.log(html);
					tbl.html(html);
				};

				me.ajax(AJAX_FILTER, data, succ, me.ajaxErr);
			},
			/*
			 * Заглушка для Ajax-success
			 */
			ajaxSucc : function(a, b, c) {
				console.log(a, b, c);
			},
			/*
			 * Заглушка для Ajax-error
			 */
			ajaxErr : function(a, b, c) {
				console.log(a, b, c);
			},
			/**
			 * AJAX-запрос
			 */
			ajax : function(addres, data, succ, err) {
				$.ajax({
					type : 'post',
					async : 'true',
					url : addres,
					data : data,
					dataType : "json",
					error : err,
					success : succ
				});
			}
		});
