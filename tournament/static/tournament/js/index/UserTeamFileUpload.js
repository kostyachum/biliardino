/**
 * @author user
 */
$(document).ready(function() {
	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#status');

	$('#file_form').ajaxForm({
		beforeSend : function() {
			status.empty();
			var percentVal = '0%';
			bar.width(percentVal)
			percent.html(percentVal);
		},
		uploadProgress : function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			bar.width(percentVal)
			percent.html(percentVal);
			//console.log(percentVal, position, total);
		},
		success : function(data) {
			var percentVal = '100%';
			bar.width(percentVal)
			percent.html(percentVal);
			console.log("success file uploading");
			$("#team_avatar").attr("src",data['url']);
			$("#avatar_link").val(data['url']);
			$("#success_upload").show("normal");
		},
		complete : function(xhr) {
			status.html(xhr.responseText);
		}
	});

});
