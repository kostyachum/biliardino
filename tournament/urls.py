from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from tournament import views, response

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'profile/(?P<player_id>\d+)/$', views.profile, name='profile'),
    url(r'manage/', include('tournament.manage_urls', namespace="manage")),    
    url(r'register/$', response.register, name='register'),
    url(r'login/$', response.user_login, name='login'),
    url(r'logout/$', response.user_logout, name='logout'),
    url(r'facebook/$', response.facebook, name='facebook'),
    url(r'vkontakte/$', response.vkontakte, name='vkontakte'),
    url(r'save_vote/(?P<tour_id>\d+)/$', response.save_vote, name='save_vote'),
    url(r'save_file/$', response.saveFileAvatar, name='save_file'),
    
    url(r'tour/(?P<tour_id>\d+)/$', views.index_tour, name='index_tour'),
    url(r'vote/(?P<tour_id>\d+)/$', views.index_vote, name='vote'),    
    url(r'round/(?P<round_id>\d+)/$', views.index_round, name='index_round'),
    url(r'team/(?P<team_id>\d+)/$', views.index_team, name='index_team'),
    url(r'error/$', views.error_login, name='error_login'),
    )
urlpatterns += patterns('',
        (r'%s(?P<path>.*)' % settings.MEDIA_URL[1:], 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

