# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

""" DATA BASE MODELS """

class Player(models.Model):
    """
    Playaer - data Base model\n    
    Uses django.auth.user
    """    
    id = models.AutoField(primary_key = True)
    """
    ID - uniq identificator\n
    Primery Key\n
    Auto add, autoincrement\n
    """
    user = models.ForeignKey(User)
    """
    User - link to django auth user\n
    userong for authorization and othe stuff
    """    
    rang = models.IntegerField(default=0)
    """
    Social network ID soon
    """
    fb_id = models.CharField(max_length=200, null=True, blank=True)
    vk_id = models.CharField(max_length=200, null=True, blank=True)
    vk_href = models.CharField(max_length=200, null=True, blank=True)
    avatar = models.CharField(max_length=200, null=True, blank=True)
    def __unicode__(self):  # Python 3: def __str__(self):
        return ('%s %s') % (self.user.first_name,self.user.last_name)
        
    
class Team(models.Model):
    """
    Team - data base model\n
    couple of player
    """
    id = models.AutoField(primary_key = True)
    """ ID """
    players = models.ManyToManyField(Player)
    """ Active, isn't?"""
    active =  models.BooleanField(default = True)
    """ Team name """
    team_name = models.CharField(max_length = 100,blank=True)
    
    avatar = models.CharField(max_length=200, null=True, blank=True)
    
    avatar_file = models.ImageField(upload_to='user_media', null=True, blank=True)
    """
    Goals
    Do not have to store
    """
    goals_me = 0
    goals_my = 0
    points = 0
    games = 0
    win = 0
    loos = 0
    dif = 0
    def __unicode__(self):  # Python 3: def __str__(self):
        return ('%s %s. - %s %s.') % (self.players.all()[0].user.first_name,self.players.all()[0].user.last_name[0], self.players.all()[1].user.first_name,self.players.all()[1].user.last_name[0])

class Tournament(models.Model):
    """
    Tournament - data base model\n
    List of rounds to play
    """
    COMING = 0
    CURRENT = 1
    FINISHED = 2
    STATUS_LIST = (
        (COMING, 'Coming'),
        (CURRENT, 'Current'),
        (FINISHED, 'Finished')
    )
    id = models.AutoField(primary_key = True)
    ""
    title = models.CharField(max_length=100,default="New tournament")
    """Players - ManyToMany field to players"""
    players = models.ManyToManyField(Player, null=True,blank=True)
    """Players, who voted"""
    voted_players = models.ManyToManyField(Player,related_name="voted_players_set",blank=True,null=True)
    """
    Team - ManyToMany
    When teams generated, they'll be added in this field
    """
    teams = models.ManyToManyField(Team, null=True, blank = True)   
    
    end_date = models.DateField(null=True, blank = True)
    
    visible = models.BooleanField(default = True)
    """Status - choice field for status"""
    status = models.SmallIntegerField(choices=STATUS_LIST, default = COMING)
    def __unicode__(self):  # Python 3: def __str__(self):
        return "%s, %s" % (self.title, self.STATUS_LIST[self.status][1])

class Round(models.Model):
    """
    Round - database model
    List of match to play
    """
    REGULAR = 0
    PLAYOFF = 1
    TYPE_LIST = (
        (REGULAR, "Regular"),
        (PLAYOFF, "Play-off")
    )
    id = models.AutoField(primary_key = True)
    """Title of round"""
    title = models.CharField(max_length = 50,default="New round")
    """Tournament realted to"""
    tournament = models.ForeignKey(Tournament)
    """Serial number"""
    serial = models.IntegerField(default = 1)   
    """Round type """
    round_type =  models.SmallIntegerField(choices=TYPE_LIST, default = REGULAR)
    
    active = models.BooleanField(default = False)
    def __unicode__(self):  # Python 3: def __str__(self):
        return "%d %s, %s" % (self.serial,self.title, self.TYPE_LIST[self.round_type][1])

class Match(models.Model):
    """
    Match - database model
    Two teams versus each other
    """    
    CANCELED = 0
    COMING = 1
    FINISHED = 2
    STATUS_LIST = (
        (CANCELED,"Canceled"),
        (COMING,"Coming"),
        (FINISHED,"Finished")
    )
    
    PLAYOFF_LIST = (
                    (0,u"Не выбрано"),
                    (8,"1/8"),
                    (4,"1/4"),
                    (2,"1/2"),
                    (1,u"Финал"),
                    (3,u"Матч на 3-е место"),
    )
    id = models.AutoField(primary_key=True)
    """Round related to """
    round = models.ForeignKey(Round)
    """Title of the match """
    title = models.CharField(max_length = 400, blank=True)
    """Match's play date"""
    play_date = models.DateField(blank=True, null=True)
    
    finish_date = models.DateField(blank=True, null=True)
    """Team A """
    team_a = models.ForeignKey(Team, related_name = "match_team_a")
    """Team B """
    team_b = models.ForeignKey(Team, related_name = "match_team_b")
    """Score of team A"""
    score_a = models.IntegerField(default = 0, null=True, blank=True)
    """Score of team B """
    score_b = models.IntegerField(default = 0, null=True, blank=True)
    """Status of match """
    status =  models.SmallIntegerField(choices=STATUS_LIST, default = COMING)   
    playoff =  models.SmallIntegerField(choices=PLAYOFF_LIST, default = 0)
    def __unicode__(self):  # Python 3: def __str__(self):
        return "M: %s, %s; %s" % (self.team_a,self.team_b,self.playoff)

class Vote(models.Model):
    player = models.ForeignKey(Player)
    tour = models.ForeignKey(Tournament)
    rate = models.IntegerField(default = 0)
    
    def __unicode__(self):  # Python 3: def __str__(self):
        return "%s, %s (%s)" % (self.player,self.rate,self.tour)
