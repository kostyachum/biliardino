# -*- coding: utf-8 -*-
from django import forms
from tournament.models import Tournament, Round, Match, Team

class RegisterForm(forms.Form):    
    login = forms.CharField(max_length=100, label=u'Логин')
    first_name = forms.CharField(label=u'Имя')
    last_name = forms.CharField(label=u'Фамилия')
    password = forms.CharField(widget=forms.PasswordInput(),label=u'Пароль')
    checkPassowrd = forms.CharField(widget=forms.PasswordInput(),label=u'Фамилия')
    email = forms.EmailField()

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100,widget=forms.TextInput(attrs={"class":"input-small","placeholder":u"Логин"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input-small","placeholder":u"Пароль"}))

class TourForm(forms.ModelForm):
    class Meta:
        model = Tournament
        fields = ['title','players', 'status','visible']

class RoundForm(forms.ModelForm):
    class Meta:
        model = Round
        fields = ['title', 'round_type']

class RoundFullForm(forms.ModelForm):
    class Meta:
        model = Round
        fields = ['title', 'round_type','tournament','serial']

class MatchForm(forms.ModelForm):
    class Meta:
        model = Match
        fields = ['title', 'team_a','team_b','play_date','status','playoff','score_a','score_b','round']
        widgets = {
            'play_date': forms.TextInput(attrs={'id':'datepicker'}),
        }
        

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ['team_name', 'players','active']