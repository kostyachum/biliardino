from tournament.models import Player, Team, Round, Tournament, Match
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models import F

class TaskClass:
    
    def countPlayers(self):        
        print "Total players: %s" %(Player.objects.count())
    """
    AddUser
    @param name = Name & Pass for new user
    @return User
    """    
    def addUser(self,name):
        usr = User()
        usr.username = name
        usr.set_password(name)
        usr.save()
        return usr
  
    """
    AddPlayer
    @param user id
    @return Player
    @return -1 on user does not exist
    """
    def addPlayer(self,user_id):
        player = Player()
        try:
            usr = User.objects.get(pk=user_id)
            player.user=usr
            player.save()
            return player

        except User.DoesNotExist:
            pass
   
    def getMatchesInTour(self,tour_id):
        matches = 0 
        rnds = Tournament.objects.get(id=tour_id).round_set.all()
        for ro in rnds:            
            matches += len(ro.match_set.all())
        return matches
            
    """
    List of matchs in tour_id, where team_id is winner
    @param tour_id
    @param team_id
    @return winner list
    """    
    def getTeamWinInTour(self,tour_id,team_id):
        tour = Tournament.objects.get(id=tour_id)
        team = Team.objects.get(id=team_id)
        win_list = []
        for round in tour.round_set.all():
            list_a = round.match_set.filter(status=2).filter(team_a=team).filter(score_a__gt=F("score_b"))
            list_b = round.match_set.filter(status=2).filter(team_b=team).filter(score_a__lt=F("score_b"))
            win_list.extend(list_a)
            win_list.extend(list_b)
        return win_list

    """
    List of matchs in tour_id, where team_id is looser 
    @param tour_id
    @param team_id
    @return looser list
    """    
    def getTeamLooseInTour(self,tour_id,team_id):
        tour = Tournament.objects.get(id=tour_id)
        team = Team.objects.get(id=team_id)
        loose_list = []
        #Select match's, where team_id has more goals then other
        for round in tour.round_set.all():
            list_a = round.match_set.filter(status=2).filter(team_a=team).filter(score_a__lt=F("score_b"))
            list_b = round.match_set.filter(status=2).filter(team_b=team).filter(score_a__gt=F("score_b"))
            loose_list.extend(list_a)
            loose_list.extend(list_b)
        return loose_list

    """
    Team's goals in tour
    @param tour_id
    @param team_id
    @return int goals
    """
    def getTeamGoalsInTour(self,tour_id,team_id):
        tour = Tournament.objects.get(id=tour_id)
        team = Team.objects.get(id=team_id)
        goals = 0
        for round in tour.round_set.all():
            for match in round.match_set.filter(status = 2,team_a = team):                
                goals += match.score_a
            for match in round.match_set.filter(status = 2,team_b = team):
                goals += match.score_b        
        return goals

    """
    Team's gools on life
    """
    def getTeamGoalAtAll(self,team_id):        
        team = Team.objects.get(id=team_id)
        goals = 0        
        for match in Match.objects.filter(status = 2,team_a = team):                
                goals += match.score_a
        for match in Match.objects.filter(status = 2,team_b = team):
                goals += match.score_b        
        return goals

    """
    Calculate team's result's in round
    """
    def getTeamResultsInRound(self,round_id,team):
        rnd = Round.objects.get(id=round_id)
        for match in rnd.match_set.filter(status = 2, team_a=team):
            team.goals_my += match.score_a
            team.goals_me += match.score_b
            team.games += 1
            if (match.score_a > match.score_b):
                team.points += 3
                team.win += 1
            if (match.score_a < match.score_b):
                team.loos +=1
        for match in rnd.match_set.filter(status = 2, team_b=team):
            team.goals_my += match.score_b
            team.goals_me += match.score_a
            team.games += 1
            if (match.score_b > match.score_a):
                team.points += 3
                team.win += 1
            if (match.score_b < match.score_a):
                team.loos +=1
        team.dif = team.goals_my-team.goals_me

    """
    Build Standings for regular round
    """
    def getTeamTableInRound(self,round_id):
        rnd = Round.objects.get(id = round_id)
        tour = rnd.tournament
        team_list = tour.teams.all()
        print("rnd: %s") %(rnd)
        print("Tour: %s") %(tour)
        print("Team list length: %s"%(team_list.count()))
        for team in team_list:
            self.getTeamResultsInRound(round_id, team)
        inorder = sorted(team_list, key=lambda team: team.dif, reverse = True)
        inorder = sorted(inorder, key=lambda team: team.points, reverse = True)
        return inorder