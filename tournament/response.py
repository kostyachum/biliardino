from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from itertools import chain
from django.core import serializers
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from tournament.forms import RegisterForm, LoginForm
from tournament.models import Player, Tournament, Match, Round, Team, Vote
from datetime import datetime
import json
import random

def register(request):
    if request.method == 'POST': # If the form has been submitted...
        form = RegisterForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            fname = form.cleaned_data['first_name']
            lname = form.cleaned_data['last_name']
            psw = form.cleaned_data['password']
            login = form.cleaned_data['login']
            mail = form.cleaned_data['email']
            usr = User.objects.create_user(login,mail,psw)
            usr.last_name = lname
            usr.first_name = fname
            usr.save()
            plr = Player(user = usr)
            plr.save()
            return redirect("/") # Redirect after POST
    else:
        form = RegisterForm() # An unbound form
        login_form = LoginForm()
    return render(request, 'tournament/register.html', {
        'form': form,
        'login_form': login_form
    })
"""
Login or registration via Facebook
"""
def facebook(request):
    facebook_id = request.POST.get("facebook_id")
    email = request.POST.get("email")
    ava = request.POST.get("photo")
    print("login via facebook")
    try:
        #print("try to find!")
        player = Player.objects.get(fb_id = facebook_id)
        player.avatar = ava
        player.save()
        usr = authenticate(username=player.fb_id, password=("salt%s")%(player.fb_id))
        login(request,usr)
        return HttpResponse(json.dumps("login"), content_type="application/json")
    
    except Player.DoesNotExist:
        #print("try to create")
        psw = ("salt%s")%(facebook_id)
        
        
        #print(psw)
        #print(user_login)
        #print(email)
        new_user = User.objects.create_user(facebook_id,email,psw)        
        new_user.first_name = request.POST.get('first_name')
        new_user.last_name = request.POST.get('last_name')
        new_user.save()
        
        new_player = Player()
        new_player.user = new_user
        new_player.fb_id = facebook_id
        new_player.avatar = ava
        new_player.save()
        
        usr = authenticate(username=facebook_id, password=psw)
        login(request,usr)
        return HttpResponse(json.dumps("register"), content_type="application/json")
    return HttpResponse(False)

"""
Login or register via Vkontakte
"""
def vkontakte(request):
    vkontakte_id = request.POST.get("vk_id")
    href = request.POST.get("href")
    ava = request.POST.get("photo")
    #print(vkontakte_id)
    #print("login via vkontakte")
    try:
        print("try to find!")
        player = Player.objects.get(vk_id = vkontakte_id)
        usr = authenticate(username=player.vk_id, password=("salt%s")%(player.vk_id))
        login(request,usr)
        return HttpResponse(json.dumps("login"), content_type="application/json")
    
    except Player.DoesNotExist:
        print("try to create")
        psw = ("salt%s")%(vkontakte_id)        
        print(psw)
        print(user_login)
        new_user = User.objects.create_user(vkontakte_id,"",psw)        
        new_user.first_name = request.POST.get('first_name')
        new_user.last_name = request.POST.get('last_name')
        new_user.save()
        
        new_player = Player()
        new_player.user = new_user
        new_player.vk_id = vkontakte_id
        new_player.avatar = ava
        new_player.vk_href = href
        new_player.save()
        
        usr = authenticate(username=vkontakte_id, password=psw)
        login(request,usr)
        return HttpResponse(json.dumps("register"), content_type="application/json")
    return HttpResponse(False)
"""
User authorization
"""
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            try:
                print("Valid form, do login!")
                usr = authenticate(username=form.cleaned_data['login'], password=form.cleaned_data['password'])
                login(request, usr)
                return redirect("/") # Redirect after POST
            except:                
                return redirect("/error")
        else:
            return redirect("/error")
    return render(request, 'tournament/index.html', {        
        'login_form': form
    })

"""
User logout
"""
def user_logout(request):
    logout(request)
    return redirect("/")

"""
Save match results
"""
def save_match_results(request):
    score_a = request.POST.get("score_a")
    score_b = request.POST.get("score_b")
    match_id = request.POST.get("match_id")
    match = Match.objects.get(id=match_id)
    match.score_a = score_a
    match.score_b = score_b
    match.finish_date = datetime.now()
    match.status = 2
    match.save()
    return HttpResponse(json.dumps("ok"), content_type="application/json")

def save_vote(request,tour_id):
    #print("save vote")
    #Take list of players
    players_list = request.POST.get("list")
    #Tournament vote
    tour = Tournament.objects.get(id = tour_id)
    #encode player list    
    lst = json.loads(players_list)
    #index pre place
    index = 100/(tour.players.count()-1)
    this_player = Player.objects.get(user=request.user)
    
    #go
    for i,plr in enumerate(reversed(lst)):
        num = i+1
        voted_count = tour.voted_players.count()
        player = Player.objects.get(id=plr)
        if player in tour.voted_players.all():
            voted_count = voted_count-1
        vote = None
        cur_rng = 0        
        try:
            vote = Vote.objects.get(player=player,tour=tour)
            cur_rng = vote.rate
        except Vote.DoesNotExist:
            vote = Vote()
            vote.player = player
            vote.tour = tour
            cur_rng = 0
        if cur_rng == 0:
            new_rng = index*num
        else:
            new_rng = ((cur_rng*voted_count)+(index*num))/(voted_count+1)
        vote.rate = new_rng
        vote.save()        
        print("%s get rang %d")%(player,new_rng)
    tour.voted_players.add(this_player)
    tour.save()
    return HttpResponse(json.dumps("ok"), content_type="application/json")
"""
Reading JSON file and send
"""
def get_standings(request):
    response_data = {}
    res = ''
    addres = ('standings/%s.json') %(request.POST.get("round_id"))
    try:
        with open(addres, 'r') as myFile:
            res = myFile.read()            
        response_data['result'] = 'success'
    except:
        response_data['result'] = 'fail'     

    return HttpResponse(json.dumps(res), content_type="application/json")

"""
Stangings for Play-off from JSON file
"""
def get_match_standings(request):
    response_data = {}
    res = ''
    norm_answer=[]
    addres = ('standings/%s.json') %(request.POST.get("round_id"))
    try:
        with open(addres, 'r') as myFile:            
            res = myFile.read()
            for line in json.loads(res):                
                line_data = []
                #print("start match id loop")
                for match_id in json.loads(line):
                    match = Match.objects.get(id=match_id)
                    match_title = ("%s vs %s")%(match.team_a, match.team_b)
                    line_data.append(match_title)
                norm_answer.append(line_data)
        response_data['result'] = 'success'
    except:
       # print("error while loading standings")
        response_data['result'] = 'fail'
    print norm_answer
    return HttpResponse(json.dumps(norm_answer), content_type="application/json")

"""
Save standings to json file
"""
def standings(request):
    response_data = {}
    a8 = request.POST.get("a8")
    a4 = request.POST.get("a4")
    a2 = request.POST.get("a2")
    a1 = request.POST.get("a1")
    json_file = [a8,a4,a2,a1] 
    addres = ('standings/%s.json') %(request.POST.get("round_id"))
    with open(addres, 'w') as outfile:
        outfile.write(json.dumps(json_file))        
    response_data['results'] = 'success'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

"""
Delete objects by ajax query
"""
def ajaxDelete(request):
    query = request.POST.get("query")
    response_data = {}
    if query == "tour":
        tour_id = request.POST.get("tour_id")
        Tournament.objects.get(id = tour_id).delete()
        response_data['result'] = "success"
    if query == "team":
        team_id = request.POST.get("team_id")
        Team.objects.get(id = team_id).delete()
        response_data['result'] = "success"
    if query == "match":
        match_id = request.POST.get("match_id")
        Match.objects.get(id = match_id).delete()
        response_data['result'] = "success"
    if query == "round":
        round_id = request.POST.get("round_id")
        Round.objects.get(id = round_id).delete()
        response_data['result'] = "success"
        
    if query == "player_from_tour":
        player_id = request.POST.get("player_id")
        tour_id = request.POST.get("tour_id")
        player = Player.objects.get(id=player_id)
        tour = Tournament.objects.get(id=tour_id)
        tour.players.remove(player)
        tour.save()
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def ajaxAdd(request):
    response_data = {}
    query = request.POST.get('query')
    if query == "addPlayerToTour":
        player_id = request.POST.get("player_id")
        tour_id = request.POST.get("tour_id")
        player= Player.objects.get(id=player_id)
        tour = Tournament.objects.get(id=tour_id)
        tour.players.add(player)
        tour.save()
        response_data['result'] = 'success'
        #print ("add player %s to tour %s")%(player,tour)
    return HttpResponse(json.dumps(response_data), content_type="application/json")
"""
Seperated generator
"""
def generator(request):
    print "separated generator"
    players = request.POST.get('players')
    players_encoded = json.loads(players)
    
    player_list = []
    loosers = []
    winners = []
    teams = []
    print "here we go"
    #Get list of players from JSON  
    for plr in players_encoded:
            player_list.append(Player.objects.get(id=plr))
    try:
        print "try tour"
        #Last tournament
        last_tour = Tournament.objects.filter(status=2).order_by("-end_date")[0]
        print "tour exist"
        #Get rank for players
        for plr in player_list:
            try:
                vote = Vote.objects.get(player = plr, tour = last_tour)
                plr.rang = vote.rate
            except Vote.DoesNotExist:
                plr.rang = 5       
    except:
        print "tour not exist"
        for plr in player_list:
            plr.rang = random.randint(1,10)
    print "after this"   
    sorted_players = sorted(player_list, key=lambda Player: Player.rang)
    mid = len(player_list)/2
    for i, plr in enumerate(sorted_players):
        if i<mid:                
            winners.append(plr)
        else:
            loosers.append(plr)
    for i in range(mid):
        teams.append([winners[i].id,loosers[mid-1-i].id])
    return HttpResponse(json.dumps(teams), content_type="application/json")

"""
Match generator
"""
def match_generator(request):
    round_id = request.POST.get("round_id")
    count = request.POST.get("count")
    rnd = Round.objects.get(id = round_id)
    teams = rnd.tournament.teams.all()
    print "Match generator" 
    
    inc = 0
    for itter in range(int(count)):
        if inc == 2: inc = 0
        inc += 1    
        """ After this each team plays with each team 1 time """
        for i,team_a in enumerate(teams):
            for k,team_b in enumerate(teams):                            
                if i>k:
                    m = Match()
                    print "inc = %s"%inc
                    if inc == 2:
                        m.title = ("%s : %s") %(team_a, team_b)
                        m.team_a = team_a
                        m.team_b = team_b
                    else:
                        m.title = ("%s : %s") %(team_b, team_a)
                        m.team_a = team_b
                        m.team_b = team_a
                    m.round = rnd
                    m.save()
                
                           
                    
    return HttpResponse(json.dumps({"success":"generated"}), content_type="application/json")
"""
Team generation here
"""
def ajaxList(request):
    print "ajax generator"
    query = request.POST.get("query")
    response_data = {}
    if query == "tour_players":
        """
        Generate teams by rang
        """        
            
        tour_id = request.POST.get("tour_id");
        tour = Tournament.objects.get(id=tour_id)        
        players = tour.players.all()
        
        try:
            last_tour = Tournament.objects.filter(status=2).order_by("-end_date")[0]
            for plr in players:
                try:
                    vote = Vote.objects.get(player = plr, tour = last_tour)
                    plr.rang = vote.rate
                except Vote.DoesNotExist:
                    plr.rang =  random.randint(1,10)  
        except:
            for plr in players:
                plr.rang = random.randint(1,10)  

         
        #average_rang = players.aggregate(Avg('rang'),Max('rang'),Min('rang'))
        #disp = average_rang['rang__max'] - average_rang['rang__min']
        #rang_list = players.values_list('rang', flat=True)

        loosers = []
        winners = []
        teams = []
        
        sorted_players = sorted(players, key=lambda Player: Player.rang)
        mid = len(players)/2
        
        #print("enumerate")
        for i, plr in enumerate(sorted_players):
            if i<mid:                
                winners.append(plr)
            else:
                loosers.append(plr)
        #print("generate teams")

        tour.teams.clear()
        tour.save()
        
#         from random import randrange
#         print randrange(10)
        for i in range(mid):            
            team = Team()
            team.team_name = ("%s - %s") %(winners[i],loosers[mid-1-i])
            team.save() #Have to save, else can't add players
            team.players.add(winners[i])
            
            teamate = loosers[mid-1-i]
            team.players.add(teamate)
            team.save()
            teams.append(team)
            tour.teams.add(team)
        tour.save()            
        response_data = serializers.serialize('json', teams)
    return HttpResponse(json.dumps(response_data), content_type="application/json")

"""
Filtered list
"""
def ajaxFilter(request):
    query = request.POST.get('query')
    search =""
    id = -1;    
    try:        
        search = request.POST.get('search')
        if search is None:
            search = ""
    except: # S
        pass
    
    try:
        id = request.POST.get("id")
        if id == "":
            id = -1
    except:
        pass
    #print("new if")
    fil = request.POST.get('filter')
    if fil == "-1":# and search == None:
        #print "fil = -1"
        if query == "tour":
            answer = Tournament.objects.all()
            
        if query == "round":
            answer = Round.objects.all()
            if id > -1:
                answer = answer.filter(tournament_id = id)
                
        if query == "match":
            answer = Match.objects.all()
            if id > -1:
                answer = answer.filter(round_id = id)
                
        if query == "team":
            answer = Team.objects.all()
            
        if query == "player":
            users = User.objects.filter(Q(first_name__icontains = search) | Q(last_name__icontains = search))     
            answer = Player.objects.filter(user__in=users)
        
        """ Search round by team"""
        if query == "matchByTeam":
            team_id = request.POST.get('team_id')
            round_id = request.POST.get('round_id')            
            team = Team.objects.get(id = team_id)
            rnd = Round.objects.get(id = round_id)
            answer = Match.objects.filter(Q(team_a = team) | Q(team_b = team)).filter(round = rnd)
        if query == "matchByPlayer":            
            player_id = request.POST.get("player_id")
            round_id = request.POST.get('round_id')
            
            rnd = Round.objects.get(id = round_id)
            player = Player.objects.get(id = player_id)            
            team = Team.objects.filter(players__in = [player])
            
            answer = Match.objects.filter(round = rnd).filter(Q(team_a__in = team) | Q(team_b__in = team))
            
    else:
        if query == "tour":    
            answer = Tournament.objects.filter(status = fil)

        if query == "round":
            answer = Round.objects.filter(round_type = fil, tournament_id = id)
            if id > -1:
                answer = answer.filter(tournament_id = id)
                
        if query == "match":
            print "NOT WORK!!!"
            if "" == search:
                answer = Match.objects.filter(status =fil,round_id = id)
            else:
                users = User.objects.filter(Q(first_name__icontains = search) | Q(last_name__icontains = search))
                answer1 = Match.objects.filter(team_b__players__user__in=users)
                answer2 = Match.objects.exclude(id__in=answer1).filter(team_a__players__user__in=users)
                        
                if fil is not None:
                    answer1 = answer1.filter(status=fil)
                    answer2 = answer2.filter(status=fil)
                    if id > -1:
                        answer1 = answer1.filter(round_id = id)
                        answer2 = answer2.filter(round_id = id)
            
                        answer = chain(answer1,answer2)
            
        if query == "team":
            users = User.objects.filter(Q(first_name__icontains = search) | Q(last_name__icontains = search))
            players = Player.objects.filter(user__in = users)
            answer = Team.objects.filter(players__in=players).order_by('id')            
    
    data = serializers.serialize('json', answer,indent = 4, relations = ('team_a','team_b','user'))
    return HttpResponse(json.dumps(data), content_type="application/json")    

def saveFileAvatar(request):
    response_data = {}
    team_id = request.POST.get("team_id")
    ava_file = request.FILES['myfile']
    path = ContentFile(ava_file.read())
    filename = ("%s")%(ava_file.name)
    team = Team.objects.get(id=team_id)
    team.avatar_file.save(filename,path)
    team.avatar = team.avatar_file.url
    
    #===========================================================================
    # #https://code.google.com/p/imageshackapi/wiki/ImageshackAPI
    # #SOME API KEY ERROR, TRY LATER
    # api_key = "0ZCJXKHW1664b27b03c4059cb32e949b4a694dfc"
    # url = "http://post.imageshack.us/upload_api.php"    
    # my_url = "gollasso.herokuapp.com%s"%team.avatar_file.url
    # print team.avatar_file.url
    # data = "key=%s&format=json&url=http://cs10817.vk.me/v10817923/cb6/lDMfGUNAvK8.jpg"%api_key
    # params = {"key": api_key, "format":"json","url": "http://cs10817.vk.me/v10817923/cb6/lDMfGUNAvK8.jpg"}
    # header = {"Content-type": "application/json"}
    # print "req"
    # try:
    #     req = requests.post(url, data, headers = header, allow_redirects=True)
    #     print req.content
    # except Exception, e:
    #     print e.message
    # print "ava file"
    #===========================================================================
    
    
    
    team.save()
    response_data['url'] = team.avatar_file.url
    return HttpResponse(json.dumps(response_data), content_type="application/json")
"""
Saving models from forms on manage panel
"""
def ajaxSave(request):
    query = request.POST.get('query')
    response_data = {}

    if (query == "tour"):
        name = request.POST.get("name")
        status = request.POST.get("status")
        tour_id =  request.POST.get("id")
        vis = request.POST.get("visible")
        tour = Tournament.objects.get(id=tour_id)
        
        if vis == "true":
            tour.visible = True
        if vis == "false":
            tour.visible = False
        #print tour.visible
        
        if tour.status != 2:
            if status == "2":
                tour.end_date = datetime.now()
         
        tour.title = name
        tour.status = status

        tour.save()
        response_data['result'] = "tour saved"
    if (query == "round"):
        name = request.POST.get("name")
        serial = request.POST.get("serial")
        round_type = request.POST.get("type")
        round_id = request.POST.get("id")
        active =  request.POST.get("active")

        rnd = Round.objects.get(id=round_id)
        rnd.title = name
        rnd.round_type = round_type
        rnd.serial = serial
        if active == "true":
            rnd.active = True
        if active == "false":
            rnd.active = False
        rnd.save()
        response_data['result'] = "round saved"
    if (query == "match"):
        name = request.POST.get("name")
        status = request.POST.get("status")
        score_a = request.POST.get("score_a")
        score_b = request.POST.get("score_b")
        playfoff = request.POST.get("playoff")
        date = None
        print "date"
        try:
            date = request.POST.get("play_date","")
            if date == "":
                date = None
            print date
        except:
            print "no date"
            pass
        match_id = request.POST.get("id")        
        match = Match.objects.get(id=match_id)
        #print(match.play_date)
        match.title = name
        match.play_date = date
        match.status = status
        match.score_a = score_a
        match.score_b = score_b
        match.playoff = playfoff
        #print(match.play_date)
        match.save()
        response_data['result'] = "match saved"

    if (query == "team"):
        team_name = request.POST.get("team_name")
        active = request.POST.get("active")
        team_id = request.POST.get("id")
        ava = request.POST.get("avatar")
        #print(active)
        tm = Team.objects.get(id=team_id)
        if active == "1":
            tm.active = True
        else:
            tm.active = False
        tm.team_name = team_name
        tm.avatar = ava
        tm.save()
        response_data['result'] = "team save"

    if (query == "player"):
        player = Player.objects.get(user_id=request.user.pk)
        #print player.id
        #print request.POST.get("player_id")
        #Only owner can change name
        if player.id == int(request.POST.get("player_id")):
            player.user.first_name = request.POST.get("first")
            player.user.last_name = request.POST.get("last")
            player.user.email = request.POST.get("email")
            player.user.save()
            player.save()
            response_data['result'] = "player saved"
        else:
            print("no")
            response_data['result'] = "0"
    return HttpResponse(json.dumps(response_data), content_type="application/json")

"""
Gathering data from Tournament wizard
!TO SLOW!
Data Base save query takes too much time
"""
def wizzard(request):
    wizTour = Tournament()
    t_title = request.POST.get("title")
    t_status = request.POST.get("status")
    players = request.POST.get("players")
    rounds = request.POST.get("rounds")
    teams = request.POST.get("teams")
    
    team_list = {}
    rounds_encoded = json.loads(rounds)
    players_encoded = json.loads(players)
    teams_encoded = json.loads(teams)

    wizTour.title = t_title
    wizTour.status = int(t_status)
    wizTour.save()
    
    """
    Create teams
    """
    for tm in teams_encoded:
        player_a = Player.objects.get(id=teams_encoded[tm]['a'])
        player_b = Player.objects.get(id=teams_encoded[tm]['b'])
        team = Team()
        team.save()
        team.players.add(player_a)
        team.players.add(player_b)
        name_a = ""
        name_b = ""

        """ Good team names generator """
        #player_a
        if player_a.user.first_name == "" or player_a.user.last_name == "":
            name_a = player_a.user.username
        else:
            name_a = ("%s %s.")%(player_a.user.first_name,player_a.user.last_name[0])
        
        #player_b
        if player_b.user.first_name == "" or player_b.user.last_name == "":
            name_b = player_b.user.username
        else:
            name_b = ("%s %s.")%(player_b.user.first_name,player_b.user.last_name[0])

        team.team_name = ("%s - %s") % (name_a, name_b)
        team.save()
        wizTour.teams.add(team)
        team_list[teams_encoded[tm]['index']] = team;
    
    

    for plr in players_encoded:
        #print("PLAYERS: %s \n") %(players_encoded[plr]['id'])
        wizTour.players.add(Player.objects.get(id=int(players_encoded[plr]['id'])))
    wizTour.save()

    for rnd in rounds_encoded:
        #Inside of round
        wizRnd = Round()
        wizRnd.title = rounds_encoded[rnd]['title']
        wizRnd.round_type = int(rounds_encoded[rnd]['type'])
        wizRnd.serial = int(rounds_encoded[rnd]['serial'])
        wizRnd.tournament = wizTour
        wizRnd.save()
        for m in rounds_encoded[rnd]['match_list']:            
            #Inside of match
            #print(rounds_encoded[rnd]['match_list'][m]['team_b'])
            #print("i want team %s") %(team_list[rounds_encoded[rnd]['match_list'][m]['team_b']])
            match = Match()
            match.title = rounds_encoded[rnd]['match_list'][m]['name']
            match.team_a = team_list[rounds_encoded[rnd]['match_list'][m]['team_a']]
            match.team_b = team_list[rounds_encoded[rnd]['match_list'][m]['team_b']]
            match.round=wizRnd            
            match.save()
            wizRnd.match_set.add(match)            
            #print(match)
        wizRnd.save()
        wizTour.save()
    response_data = {"RESPONSE":"good","players":players}
    return HttpResponse(json.dumps(response_data), content_type="application/json")