from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.template import Library
from tournament.task import TaskClass

import json

register = Library()

def jsonify(_object):
	print("jsonify")
	if isinstance(_object, QuerySet):
		return serialize('json', _object)
	return json.dumps(object)

def key(d, key_name):
	return d[key_name][1]

def index(d,index):
	return d[index]

def match_index(d):
	return [d[0]]

def player_user_id(d,index):
	return d[index].user.id
def key_ava(d, key_name):
	print(d[key_name])
	return d[key_name].avatar

def in_status(_set,status):
	instatus = _set.filter(status=status)
	return instatus[:10]

def playoff_list(_set,index):
	txt= [x for x in _set if x[0] == index]
	return txt[0][1] 

def playoff_filter(_set, playoff):
	return _set.filter(playoff=playoff)

def playoff_table_filter(_set,playoff):
	print "playoff table filter"
	p_match = None	
	_set = _set.filter(playoff=playoff)
	
	_set = sorted(_set,key=lambda x: x.team_a.id)	
	_set = sorted(_set,key=lambda x: x.team_b.id)
	new_set =[]
	teams = 0	
	for match in _set:
		if p_match:
			if (((p_match.team_a.id == match.team_a.id) or (p_match.team_a.id == match.team_b.id)) and ((p_match.team_b.id == match.team_a.id) or (p_match.team_b.id == match.team_b.id))):
				new_set[teams].append(match)
			else:
				teams = teams+1
				new_set.append([])		
				new_set[teams].append(match)
				print "new_set:"
				print new_set[teams]
				
		else:
			new_set.append([])	
			new_set[teams].append(match)
		p_match = match
	for i,match_set in enumerate(new_set):
		new_set[i] = sorted(new_set[i],key=lambda x: x.status, reverse = True)
	return new_set

def set_in_status_length(_set, status):
	instatus = _set.filter(status=status)
	return instatus.count()

def playoff_standings(_rnd):
	response_data = {}
	res = ''
	addres = ('standings/%s.json') %(_rnd.id)
	try:
		with open(addres, 'r') as myFile:
			res = myFile.read()
			response_data['data'] = res
		response_data['result'] = 'success'
	except:
		response_data['result'] = 'fail'
	return response_data

def round_table(rnd):
	tsk = TaskClass()
	return tsk.getTeamTableInRound(rnd.id)

register.filter('player_user_id',player_user_id)
register.filter('key', key)
register.filter('index', index)
register.filter('match_index', match_index)
register.filter('key_ava', key_ava)
register.filter('playoff_filter', playoff_filter)
register.filter('playoff_table_filter', playoff_table_filter)
register.filter('in_status', in_status)
register.filter('playoff_list', playoff_list)
register.filter('jsonify', jsonify)
register.filter('round_table', round_table)
register.filter('playoff_standings', playoff_standings)
register.filter('set_in_status_length', set_in_status_length)